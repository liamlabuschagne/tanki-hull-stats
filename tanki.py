import csv
hulls = []


class Hull:

    def __init__(self, name, price, protection, topSpeed, turningSpeed, weight, power):
        self.name = name
        self.protection = float(protection)
        self.topSpeed = float(topSpeed)
        self.turningSpeed = float(turningSpeed)
        self.weight = float(weight)
        self.power = float(power)
        self.price = float(price)
        self.points = 0


with open('hulls.csv', 'rb') as csvfile:
    reader = csv.reader(csvfile, delimiter=',')
    firstRow = True
    for row in reader:
        if firstRow:
            firstRow = False
            continue
        hull = Hull(row[0], row[1], row[2], row[3], row[4], row[5], row[6])
        hulls.append(hull)


def sortPropertyInHulls(property, descending=True):
    print("Sorting: "+property)
    values = []
    for hull in hulls:
        values.append(eval("hull."+property))
    values.sort(reverse=descending)
    print("Sorted values: "+str(values))
    i = len(values)
    for value in values:
        for hull in hulls:
            if value == eval("hull."+property):
                hull.points = hull.points + i
                print(str((len(values)+1)-i) + ": " + hull.name +
                      " got " + str(i) + " points with a " + property + " of " + str(value))
        i = i - 1
    print("-------------------------------------------------------------------------")


sortPropertyInHulls("price", False)
sortPropertyInHulls("protection")
sortPropertyInHulls("topSpeed")
sortPropertyInHulls("turningSpeed")
sortPropertyInHulls("weight")
sortPropertyInHulls("power")
print("Final Placings")
print("-------------------------------------------------------------------------")
sortPropertyInHulls("points")
